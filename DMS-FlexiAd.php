<?php
/**
 * Plugin Name: DMS Flexible Ad Module
 * Plugin URI: https://digitalmediasolutions.com/
 * Description: This flexible Ad Module will allow you to obtain advertisements from the Push Network. You will earn revenue based on your user's engagement. All you need to do is figure out how and where you'd like ads placed on your site. Use the simple tool below to create ads, place the shortcode on your site, and you're all set! The plugin provides plenty of opportunities for optimization, and our docs can help provide some instruction on common techniques for providing a great user experience and making money on your site.
 * Version: 1.0
 * Author: Digital Media Solutions®
 * Author URI: https://digitalmediasolutions.com/
 */
/* Plugin Activation Hook */
function flexi_Ad_activation()
{
    my_DMS_widget();
}
register_activation_hook(__FILE__, 'flexi_Ad_activation');
/* Plugin Deactivation Hook */
function flexi_Ad_deactivation()
{
}
register_deactivation_hook(__FILE__, 'flexi_Ad_deactivation');
register_activation_hook(__FILE__, array('Install', 'Flexi_Ad_Install'));
register_activation_hook(__FILE__, array('InstallData', 'Flexi_Ad_Install_data'));
register_deactivation_hook(__FILE__, array('DeleteTable', 'delete_Flexi_Ad_table'));
/* Create Database Table */
global $DMS_flexi_db_version;
$DMS_flexi_db_version = '1.0';
function get_user_ip()
{
    $ip = '';
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function get_uid()
{
    return get_user_ip();
}
//Get Geo location
$dms_vis_ip = get_user_ip();
//$dmsGeoData = json_decode(file_get_contents("https://ipinfo.io/{$dms_vis_ip}/json?token=c957df26640ee0"));
$dmsGeoData =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$dms_vis_ip));
//Get Geo location

/* End Drop database table */
include "inc/flexi_menu.php";
include "inc/install.php";
include "inc/installation_data.php";
include "inc/delete_deactivation.php";
include "inc/create_new_ad.php";
include "inc/configuration.php";
include "inc/settings.php";
include "inc/custom-widget.php";
include "inc/widget.php";
add_action('wp_enqueue_scripts', 'swp_register_plugin_styles');
function swp_register_plugin_styles()
{
    wp_register_style('DMS-Flexi-Ad-style', plugins_url('DMS-flexi-module/css/pluginStyle.css'));
    wp_enqueue_style('DMS-Flexi-Ad-style');
    wp_register_script('DMS-Flexi-Ad-showad', plugins_url('DMS-flexi-module/js/show_ad.js'));
    wp_enqueue_script('DMS-Flexi-Ad-showad');
    wp_register_style('DMS-Flexi-Ad-style1', plugins_url('DMS-flexi-module/css/bootstrap.min.css'));
    wp_enqueue_style('DMS-Flexi-Ad-style1');
    wp_register_style('DMS-Flexi-Ad-fontAwesome-style', plugins_url('DMS-flexi-module/css/font-awesome.min.css'));
    wp_enqueue_style('DMS-Flexi-Ad-fontAwesome-style');
    wp_register_script('DMS-Flexi-Ad-script', plugins_url('DMS-flexi-module/js/bootstrap.min.js'));
    wp_enqueue_script('DMS-Flexi-Ad-script');
    wp_register_style('DMS-Flexi-Ad-style-admin-widget-style0', plugins_url('DMS-flexi-module/css/admin-widget-style.css'));
    wp_enqueue_style('DMS-Flexi-Ad-style-admin-widget-style0'); //admin-wiget area style

}
/* Add plugin into wordpress menu */
add_action('admin_menu', 'Flexi_Advertisement_setup_menu');
function Flexi_Advertisement_setup_menu()
{
    wp_register_style('DMS-Flexi-Ad-style', plugins_url('DMS-flexi-module/css/bootstrap.min.css'));
    wp_enqueue_style('DMS-Flexi-Ad-style');
    wp_register_style('DMS-Flexi-Ad-Dash-style', plugins_url('DMS-flexi-module/css/flexi-dashboard.css'));
    wp_enqueue_style('DMS-Flexi-Ad-Dash-style');
    wp_register_style('DMS-Flexi-Ad-fontAwesome-fonticonstyle', plugins_url('DMS-flexi-module/css/font-awesome.min.css'));
    wp_enqueue_style('DMS-Flexi-Ad-fontAwesome-fonticonstyle');
    wp_register_script('DMS-Flexi-Ad-script', plugins_url('DMS-flexi-module/js/bootstrap.min.js'));
    wp_enqueue_script('DMS-Flexi-Ad-script');
    wp_register_script('DMS-Flexi-Ad-jqueryslim-script', plugins_url('DMS-flexi-module/js/jquery-3.3.1.min.js'));
    wp_enqueue_script('DMS-Flexi-Ad-jqueryslim-script');
    wp_register_script('DMS-Flexi-Ad-popper-script', plugins_url('DMS-flexi-module/js/popper.min.js'));
    wp_enqueue_script('DMS-Flexi-Ad-popper-script');
    wp_register_script('DMS-Flexi-Ad-copyclip-script', plugins_url('DMS-flexi-module/js/copyclipboard.js'));
    wp_enqueue_script('DMS-Flexi-Ad-copyclip-script');
    wp_register_script('DMS-Flexi-Ad-copyclip-script', plugins_url('DMS-flexi-module/js/jquery.min.js'));
    wp_enqueue_script('DMS-Flexi-Ad-jquery-script');
    wp_register_script('DMS-Flexi-Ad-jqueryscript-script', plugins_url('DMS-flexi-module/js/jquery.js'));
    wp_enqueue_script('DMS-Flexi-Ad-jqueryscript-script');
    wp_register_script('DMS-Flexi-Ad-jqueryscript-3.1.1-script', plugins_url('DMS-flexi-module/js/jquery.min-3.1.1.js'));
    wp_enqueue_script('DMS-Flexi-Ad-jqueryscript-3.1.1-script');
    wp_register_script('DMS-Flexi-Ad-colorpicker-script', plugins_url('DMS-flexi-module/js/color-picker.js'));
    wp_enqueue_script('DMS-Flexi-Ad-colorpicker-script');
    wp_register_script('DMS-Flexi-Ad-widgettab-script', plugins_url('DMS-flexi-module/js/widget-tab.js'));
    wp_enqueue_script('DMS-Flexi-Ad-widgettab-script');
    wp_register_script('DMS-Flexi-Ad-widgettab-multiselect-script', plugins_url('DMS-flexi-module/js/multi_option_select.js'));
    wp_enqueue_script('DMS-Flexi-Ad-widgettab-multiselect-script');
    wp_register_style('DMS-Flexi-Ad-style-admin-widget-style1', plugins_url('DMS-flexi-module/css/admin-widget-style.css'));
    wp_enqueue_style('DMS-Flexi-Ad-style-admin-widget-style1'); //admin-wiget area style
}
function getAPIData()
{
    global $wpdb;
    $dmsApiSettings = '';
    $dmsTableName = $wpdb->prefix . 'DMS_ApiSettings';
    $dmsApiSettings = $wpdb->get_results("SELECT * FROM $dmsTableName");
    return $dmsApiSettings;
}
function dmsFlexiDashboard()
{ ?>
      <div class="container">
          <div class="row pluginHeader">
              <div class="col-lg-2">
                  <img src="<?php echo plugins_url('image/dms-logo.png', __FILE__); ?>" border="0" class="headerLogo" />
              </div>
              <div class="col-lg-10">
                  <h1 class="adminHeader">
                      <?php esc_html_e('Welcome to DMS flexible Advertisement.', 'DMS Flexi Ad');?>
                  </h1>
              </div>
          </div>
          <div class="row pluginDescription">
              <div class="col-lg-12">
                  <h2 class="descriptionHeader">Plugin Description</h2>
                  <hr>
                  <p>
                      <strong>Are you ready to monetize your site?</strong> this flexible Ad Module will allow you to
                      obtain advertisements from the Push Network. You will earn revenue based on your user's
                      engagement.
                  </p>
                  <p><strong>How does it work?</strong> all you need to do is figure out how and where you'd like ads
                      placed on your site. Use the simple tool below to create ads, place the shortcode on your site,
                      and you're all set! The plugin provides plenty of opportunities for optimization, and our docs can
                      help provide some instruction on common techniques for providing a great user experience and
                      making money on your site.
                  </p>
                  <p>Be sure to create an Account if you have not already, and begin making advertising revenue like the
                      post</p>
              </div>
          </div>
          <div class="row">
              <div class="col-lg-12">
                  <form name="form" action="" method="POST">
                      <a href="<?php echo admin_url('admin.php?page=create-new-Ad'); ?>">
                        <?php
                        $dmsApiSettings = getAPIData();
                        $dmsApiPublicKey = isset($dmsApiSettings[0]->publickey)?$dmsApiSettings[0]->publickey:'';
                        if ($dmsApiPublicKey == '') {
                               echo '<button type="button" class="btn btn-success tooltip1 btn-sm float-right mb-3" disabled = "disabled" style="position: relative;"><i class="fas fa-plus-square"></i> Create New<span class="tooltip1 tooltiptext">
                                After insert the configuration details create new button is enabled.
                                </span></button></a>';
                        } else {
                            echo '<button type="button" class="btn btn-success btn-sm float-right mb-3" style="position: relative;"><i
                                                      class="fas fa-plus-square"></i> Create New</button></a>';
                        }
                        ?>
                    <table class="table table-bordered adminTable">
                        <tr>
                          <th>S.No</th>
                          <th>Ad Name</th>
                          <th>Vertical</th>
                          <th>Shortcode</th>
                          <th>Author</th>
                          <th>Action</th>
                        </tr>
                        <?php
                        $url = "https://services.pushnetwork.com/api/content/verticals?publickey=ljacy9h3&domain=https://1.topfinancefacts.com";
                        $crl = curl_init();
                        curl_setopt($crl, CURLOPT_URL, $url);
                        curl_setopt($crl, CURLOPT_FRESH_CONNECT, true);
                        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
                        $response = curl_exec($crl);
                        if (!$response) {
                            die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
                        }
                        curl_close($crl);
                        $json = $response;
                        $json_data = json_decode($json, true);
                        $var = $json_data['verticals'];
                        global $wpdb;
                        $dmsTableName = $wpdb->prefix . 'DMS_FlexiAd';
                        $id = '';
                        $adname = '';
                        $identifier = '';
                        $adplacement = '';
                        $vertical = '';
                        $shortcode = '';
                        $current_user_name = '';
                        if (isset($_POST['delete'])) {
                            $wpdb->delete($dmsTableName, array(
                                'id' => $_POST['delete'],
                            ));
                            echo '<div class="alert alert-primary" role="alert">Shortcode Deleted Successfully</div>';
                        }
                        if (isset($_POST['submit'])) {
                            $shortcode = stripslashes($_POST['shortcode']);
                            $result = $wpdb->update($dmsTableName, array(
                                'adname' => $_POST['adname'],
                                'vertical' => $_POST['vertical'],
                                'identifier' => $_POST['identifier'],
                                'adplacement' => $_POST['adplacement'],
                                'shortcode' => $shortcode,
                            ), array(
                                'id' => $_POST['id'],
                            ));
                            echo '<div class="alert alert-warning" role="alert" style="position: relative; float: left; width: 100%;">Shortcode Updated Successfully</div>';
                        }
                        $result = $wpdb->get_results("SELECT * FROM $dmsTableName");
                        //echo '<pre>'; print_r($result);die;
                        foreach ($result as $value) {
                        ?>
                    <tr>
                        <td><?php echo $value->id; ?></td>
                        <td><?php echo $value->adname; ?></td>
                        <td><?php
                            if ($value->vertical != 0) {
                                foreach ($var as $val) {
                                    if ($val['id'] == $value->vertical) {
                                        echo $val['label'];
                                    }
                                }
                            } else {
                                echo "No Preference";
                            }?>
                        </td>
                        <td><?php echo stripslashes($value->shortcode); ?></td>
                        <td><?php echo $value->current_user_name; ?></td>
                        <td><button type="button" class="updateButton" name="update"
                              onclick="dmsGetForm(<?php echo $value->id; ?>)" value="<?php echo $value->id; ?>"><i
                                  class="fas fa-edit text-warning"></i></button> |
                            <button class="deleteButton" name="delete" value="<?php echo $value->id; ?>"><i
                                  class="fas fa-trash text-danger"></i></button>
                        </td>
                    </tr>
                <?php
                }?>
                </table>
            </form>
        </div>
        <div id="Editform" class="col-lg-12"></div>
            <div class="col-lg-12">
                <h2 class="descriptionHeader">Analysis Report</h2>
                <hr>
            </div>
            <div class="col-lg-4">
                <!-- Header Card -->
                <div class="card chart-card headerCard">
                    <!-- Card content -->
                    <div class="card-body pb-0">
                        <!-- Title -->
                        <h4 class="card-title font-weight-bold text-center headerCardTitle">Header Ad Clicks</h4>
                        <hr>
                        <div class="justify-content-between">
                            <p class="display-4 align-self-end text-center sidebarCardTitle">1590</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Header Card End -->
            <!-- Content Card -->
            <div class="col-lg-4">
                <div class="card chart-card contentCard">
                    <!-- Card content -->
                    <div class="card-body pb-0">
                        <!-- Title -->
                        <h4 class="card-title font-weight-bold text-center">Content Ad Clicks</h4>
                        <hr>
                        <div class="justify-content-between">
                            <p class="display-4 align-self-end text-center">1000</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content Card End -->

            <!--Sidebar Card -->
            <div class="col-lg-4">
                <!-- Card -->
                <div class="card chart-card sidebarCard">
                    <!-- Card content -->
                    <div class="card-body pb-0">
                        <!-- Title -->
                        <h4 class="card-title font-weight-bold text-center sidebarCardTitle">Sidebar Ad Clicks</h4>
                        <hr>
                        <div class="justify-content-between">
                            <p class="display-4 text-center sidebarCardTitle">1000</p>
                        </div>
                    </div>
                </div>
                <!-- Card -->
            </div>
        </div>
    </div><!--
      <script type="text/javascript">
        function dmsGetForm(id) {
            var url = "<?=plugins_url();?>//DMS-flexi-module/template-edit.php";
            $.post(url, {
                    id: id
                },
                function(response) {
                    $('#Editform').html(response);
                });
        }
      </script>-->
    <?php
}
//$button_settings = unserialize(str_replace("\n", "\r" ,get_option('button_settings')));
function rand_string($length)
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    return substr(str_shuffle($chars), 0, $length);
}
/* Hide advertisement*/
function hide_sidebar_ad_banner_for_specific_pages(){
     $pages_to_show_ad_banner_in_sidebar = unserialize($pages_to_show_ad_banner);
    if(!in_array($page_id, $pages_to_show_ad_banner_in_sidebar)){
        wp_enqueue_style( 'main', get_template_directory_uri() . '/style.css' );
        wp_enqueue_style( 'custom', '/wp-content/themes/ad_banner_fold/hide_sidebar_ad_banner_for_specific_pages.css' );
    }
}
add_action('wp_enqueue_scripts', 'hide_sidebar_ad_banner_for_specific_pages');
/* Hide advertisement*/

global $wpdb;
$button_settings = unserialize(str_replace("\n", "\r", get_option('button_settings')));
//echo '<pre>'; print_r($button_settings);die;
$button_color = strlen($button_settings['button_bg_color'])!=0?$button_settings['button_bg_color']:'#fb6260';
$button_text_color = strlen($button_settings['button_text_color'])!=0?$button_settings['button_text_color']:'#FFFFFF';
$dmsApiSettings = getAPIData();
$dmsApiSettings = isset($dmsApiSettings)?$dmsApiPublicKey:'';
$dmsTableName = $wpdb->prefix . 'DMS_postdata';
$dmsPostData = $wpdb->get_results("SELECT * FROM $dmsTableName");
$dmsPostData = isset($dmsPostData)?$dmsPostData:'';
if (count($dmsApiSettings) != 0 && count($dmsPostData) != 0) { 
    ?>
      <input type='hidden' id='dms_privatekey' value='<?php echo $dmsApiSettings[0]->privatekey; ?>'>
      <input type='hidden' id='dms_publickey' value="<?php echo $dmsApiSettings[0]->publickey; ?>">
      <input type='hidden' id='dms_domain' value="<?php echo $dmsPostData[0]->domain; ?>">
      <input type='hidden' id='dms_verticaldata' value="<?php echo $dmsPostData[0]->vertical; ?>">
      <input type='hidden' id='dms_ipaddress' value="<?php echo get_uid(); ?>">
      <input type='hidden' id='dms_geo' value="<?php echo $dmsGeoData['geoplugin_countryCode']; ?>">
      <input type='hidden' id='button_color' value="<?php echo $button_color; ?>">
      <input type='hidden' id='button_text_color' value="<?php echo $button_text_color; ?>">

  <?php
}

add_filter('widget_title', 'remove_widget_title');
function remove_widget_title($widget_title)
{
    if (substr($widget_title, 0, 1) == '!') {
        return;
    } else {
        return ($widget_title);
    }

}
?>