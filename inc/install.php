<?php
/**
 * 
 */
class Install
{
  function Flexi_Ad_Install()
{
    global $wpdb;
    global $DMS_flexi_db_version;
    $dmsTableName = $wpdb->prefix . 'DMS_FlexiAd';
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $dmsTableName (
          id mediumint(9) NOT NULL AUTO_INCREMENT,
              adname tinytext NOT NULL,
              vertical int NOT NULL,
              identifier text NOT NULL,
              adplacement text NOT NULL,
              pages int NOT NULL,
              shortcode text NOT NULL,
              current_user_name text NOT NULL,
              created_at datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
              updated_at datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
          PRIMARY KEY  (id)) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('DMS_flexi_db_version', $DMS_flexi_db_version);
    $dmsTableName = $wpdb->prefix . 'DMS_postdata';
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $dmsTableName (
          id mediumint(9) NOT NULL AUTO_INCREMENT,
          domain tinytext NOT NULL,
          vertical int NOT NULL,
          current_user_name text NOT NULL,
          created_at datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
          updated_at datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
          PRIMARY KEY  (id)
        ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('DMS_flexi_db_version', $DMS_flexi_db_version);
    $dmsTableName = $wpdb->prefix . 'DMS_ApiSettings';
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $dmsTableName (
          id mediumint(9) NOT NULL AUTO_INCREMENT,
          publickey varchar(50) NOT NULL DEFAULT '',
          privatekey varchar(100) NOT NULL DEFAULT '',
          current_user_name varchar(100) NOT NULL DEFAULT '',
          created_at datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
          updated_at datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
          PRIMARY KEY  (id)
        ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('DMS_flexi_db_version', $DMS_flexi_db_version);
}  
}

