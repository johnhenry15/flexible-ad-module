<?php
function dmsConfiguration()
{
?>
<div class="container">
		 <div class="row pluginHeader">
	
	<div class="col-lg-2">
			<img src="<?php echo plugins_url('../image/dms-logo.png', __FILE__); ?>" border="0" class="headerLogo"/>	    
	</div>
	<div class="col-lg-10">
		<h1 class="adminHeader">
      <?php esc_html_e('Welcome to DMS flexible Advertisement.', 'DMS Flexi Ad'); ?>
		</h1>
	</div>
	</div>
	<div class="row pluginDescription">
	<div class="col-lg-12">
	<h2 class="descriptionHeader">Configuration Settings</h2>
	<hr>
	</div>
	</div>
	<?php
    $current_user = wp_get_current_user();
    $current_user_name = $current_user->display_name;
    if (isset($_POST['submit']))
    {
        global $wpdb;
        $publickey = $_POST['publickey'];
        $privatekey = $_POST['privatekey'];
        if (empty($publickey) || empty($privatekey))
        {
            echo '<div class="alert alert-danger" role="alert">Shortcode Not Inserted. Need to fill the form fields</div>';
        }
        else
        {
            $formdata = array(
                'publickey' => trim($publickey),
                'privatekey' => trim($privatekey),
                'current_user_name' => $current_user_name,
                'created_at' => current_time('mysql') ,
                'updated_at' => current_time('mysql')
            );
            $dmsTableName = $wpdb->prefix . 'DMS_ApiSettings';
            $data = $wpdb->insert($dmsTableName, $formdata);
            echo '<div class="alert alert-primary" role="alert">Advertisement Settings Inserted Successfully</div>';
            $url = get_site_url() . "/wp-admin/admin.php?page=Flexi-Advertisement";
            wp_redirect($url);
            exit();
      }
    }
    if (isset($_POST['update']))
    {
        global $wpdb;
        $publickey = $_POST['publickey'];
        $privatekey = $_POST['privatekey'];
        if (empty($publickey) || empty($privatekey))
        {
            echo '<div class="alert alert-danger" role="alert">Advertisement Settings Not Inserted. Need to fill the form fields</div>';
        }
        else
        {
            $formdata = array(
                'publickey' => trim($publickey),
                'privatekey' => trim($privatekey),
                'current_user_name' => $current_user_name,
                'created_at' => current_time('mysql') ,
                'updated_at' => current_time('mysql')
            );
            $dmsTableName = $wpdb->prefix . 'DMS_ApiSettings';
            $id = $_POST['id'];
            $where = array(
                'id' => $id
            );
            $updated = $wpdb->update($dmsTableName, $formdata, $where);
            echo '<div class="alert alert-primary" role="alert">Advertisement Settings Updated Successfully</div>';
        }
    }
    global $wpdb;
    $dmsTableName = $wpdb->prefix . 'DMS_ApiSettings';
    $dmsApiSettings = $wpdb->get_results("SELECT * FROM $dmsTableName");
    $num_rows = count($dmsApiSettings);
    if ($num_rows != "")
    { ?>
    <form name="form" id="form" action="" method="POST">
    <input id="id" name="id" type="hidden" value="<?php echo $dmsApiSettings[0]->id; ?>">
          <div class="form-group row formBottom">
              <label for="Push Network User ID" class="col-lg-2 col-form-label">Public Key<span class="required">*</span></label>
            <input type="text" class="form-control col-lg-5" name="publickey" id="publickey" aria-describedby="AdHelp" value="<?php echo $dmsApiSettings[0]->publickey; ?>">  &nbsp;&nbsp;<span class="Adnameerrmsg"></span>
            </div>
            <div class="form-group row formBottom">
              <label for="Push Network Private API Key" class="col-lg-2 col-form-label">Private Key<span class="required">*</span></label>
            <input type="text" class="form-control col-lg-5" name="privatekey" id="privatekey" aria-describedby="AdHelp"  value="<?php echo $dmsApiSettings[0]->privatekey; ?>">  &nbsp;&nbsp;<span class="Adnameerrmsg"></span>
            </div>
            <button name="update" type="submit" class="btn btn-primary">update Changes</button>
            </form>
<?php
    }
    else
    { ?>
	<form name="form" id="form" action="" method="POST">
          <div class="form-group row formBottom">
		      <label for="Push Network User ID" class="col-lg-2 col-form-label">Public Key<span class="required">*</span></label>
            <input type="text" class="form-control col-lg-5" name="publickey" id="publickey" aria-describedby="AdHelp" >  &nbsp;&nbsp;<span class="Adnameerrmsg"></span>
			</div>
            <div class="form-group row formBottom">
		      <label for="Push Network Private API Key" class="col-lg-2 col-form-label">Private Key<span class="required">*</span></label>
            <input type="text" class="form-control col-lg-5" name="privatekey" id="privatekey" aria-describedby="AdHelp">  &nbsp;&nbsp;<span class="Adnameerrmsg"></span>
			</div>
            <button name="submit" type="submit" class="btn btn-primary">Save Changes</button>
            </form>
<?php
    }
} ?>