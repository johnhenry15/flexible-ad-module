<?php
function dmsCreateNewAd()
{
?>
       <div class="container">
		 <div class="row pluginHeader">
	
	<div class="col-lg-2">
			<img src="<?php echo plugins_url('../image/dms-logo.png', __FILE__); ?>" border="0" class="headerLogo"/>	    
	</div>
	<div class="col-lg-10">
		<h1 class="adminHeader">
            <?php esc_html_e('Welcome to DMS flexible Advertisement.', 'Pushpros Flexi Ad'); ?>
		</h1>
	</div>
	</div>
	<div class="row pluginDescription">
	<div class="col-lg-12">
	<h2 class="descriptionHeader">Create New Advertisement</h2>
	<hr>
	</div>
	</div>
	<div class="container">
    <div class="row">
      <div class="col-md-12">
        <?php
    $url = "https://services.pushnetwork.com/api/content/verticals?publickey=ljacy9h3&domain=https://1.topfinancefacts.com";
    $crl = curl_init();
    curl_setopt($crl, CURLOPT_URL, $url);
    curl_setopt($crl, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($crl);
    if (!$response)
    {
        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }
    curl_close($crl);
    $json = $response;
    $json_data = json_decode($json, true);
    $var = $json_data['verticals'];
    $current_user = wp_get_current_user();
    $current_user_name = $current_user->display_name;
    if (isset($_POST['submit']))
    {
        global $wpdb;
        $adname = $_POST['adname'];
        $vertical = $_POST['vertical'];
        $identifier = $_POST['identifier'];
        $adplacement = $_POST['adplacement'];
        $pagesarr = $_POST['pages'];
        $pagevalues=  implode(",", $pagesarr);
        $shortcode = $_POST['shortcode'];
        if (empty($identifier) || empty($shortcode) || empty($adname) || empty($adplacement))
        {
            echo '<div class="alert alert-danger" role="alert">Shortcode Not Inserted. Need to fill the form fields</div>';
        }
        else
        {
            $formdata = array(
                'adname' => $adname,
                'vertical' => $vertical,
                'identifier' => $identifier,
                'adplacement' => $adplacement,
                'pages' => $pagevalues,
                'shortcode' => stripslashes($shortcode) ,
                'current_user_name' => $current_user_name,
                'created_at' => current_time('mysql') ,
                'updated_at' => current_time('mysql')
            );
            $dmsTableName = $wpdb->prefix . 'DMS_FlexiAd';
            $data = $wpdb->insert($dmsTableName, $formdata);
            echo '<div class="alert alert-primary" role="alert">Shortcode Inserted Successfully</div>';
            $url = get_site_url() . "/wp-admin/admin.php?page=Flexi-Advertisement";
            wp_redirect($url);
            exit();
        }
    }
?>
<form name="form" id="form" action="" method="POST" autocomplete="off">
          <div class="form-group row formBottom">
		      <label for="Advertisement Name" class="col-lg-2 col-form-label">Ad Name <span class="required">*</span></label>
            <input type="text" class="form-control col-lg-5" name="adname" id="adname" aria-describedby="AdHelp">  &nbsp;&nbsp;<span class="Adnameerrmsg"></span>
			</div>
          <div class="form-group row formBottom">
            <label for="vertical" class="col-lg-2 col-form-label">Vertical <span class="required">*</span></label>
			<span class="input-help" style="width: 462px;">
            <select class="form-control col-lg-6" id="vertical" name="vertical" style="max-width: 29rem !important;">
			<option value="0" selected="selected">No Preference</option>
            <?php
    usort($var, function ($item1, $item2)
    {
        if ($item1['score'] == $item2['score']) return 0;
        return $item1['score'] > $item2['score'] ? -1 : 1;
    });
?>
			<?php for ($i = 0;$i < count($var);$i++)
    { ?>
			<option value="<?php echo $var[$i]['id']; ?>"><?php echo $var[$i]['label']; ?> <?php for ($j = 0;$j < $var[$i]['score'];$j++)
        {
            echo "$";
        } ?></option>
			
			<?php
    } ?>
		  </select>
            <small id="AdHelp" class="form-text text-muted">select the type of ad content to deliver, e.g. Finance, Triva, Lifestyle</small>
			</span>
          </div>
          <div class="form-group row formBottom">
            <label for="Identifier"class="col-lg-2 col-form-label">Identifier <span class="required">*</span></label>
			<span class="input-help">
            <input type="text" class="form-control col-lg-6" name="identifier" id="identifier" aria-describedby="IdHelp" style="width: 925px;">
            <small id="IdHelp" class="form-text text-muted">Unique Identifier to be used in the shortcode</small>
			</span>
          </div>
            <div class="form-group row formBottom">
            <label for="Identifier"class="col-lg-2 col-form-label">Placement <span class="required">*</span></label>
			<span class="input-help">
            <input type="text" class="form-control col-lg-6" name="adplacement" id="adplacement" aria-describedby="IdHelp" style="width: 925px;">
            <small id="IdHelp" class="form-text text-muted">Placement to be used in the shortcode</small>
			</span>
          </div>
          <div class="form-group row formBottom">
            <label for="Identifier"class="col-lg-2 col-form-label">Pages <span class="required">*</span></label>
            <span class="input-help">
           <?php 
$pages = get_pages(); 
$checkfield ='';
 ?>
<?php 
foreach ($pages as $page_data) {
?> 
<div class="form-check">
  <input id="page_<?php echo $page_data->ID; ?>" type="checkbox" name="pages[]" value="<?php echo $page_data->ID; ?>" <?php if ( in_array($page_data->ID, (array) $checkfield) ) { ?> checked <?php } ?>/> <label for="page_<?php echo $page_data->ID; ?>"><?php echo $page_data->post_title; ?></label>
</div>
<?php } ?>
            <small id="IdHelp" class="form-text text-muted">Placement to be used in the shortcode</small>
            </span>
          </div>
          


          <div class="form-group row formBottom">
            <label for="Shorcode" class="col-lg-2 col-form-label">Shortcode <span class="required">*</span></label>
			   <span class="input-help">
            <div class="input-group mb-3 col-sm-12 formPadding">
              <input type="text" class="form-control col-lg-12" value="[dms-flexible-ad id='' placement='' vertical='']" name="shortcode" id="Shorcode" aria-describedby="ShortcodeHelp" readonly>
              <div class="input-group-append">
                <input type="button" onclick="copyShortcode()" value="copy" class="btn btn-primary">
              </div>
            </div>
            <small id="ShortcodeHelp" class="form-text text-muted">Click to Copy, then Paste this shortcode into your
              page,post,sidebar,footer,etc.</small>
			  </span>
          </div>
		<button name="submit" type="submit" class="btn btn-primary">Save Changes</button>
        </form>
      </div>
    </div>
  </div>
  </div>
  <?php
} ?>