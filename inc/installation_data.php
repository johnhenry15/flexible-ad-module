<?php
/**
 * 
 */
class InstallData
{
    function Flexi_Ad_Install_data()
{
    global $wpdb;
    $current_user = wp_get_current_user();
    $current_user_name = $current_user->display_name;
    $ad_name = "My Default Ad";
    $vertical = "0";
    $identifier = "my-default-ad";
    $adplacement = "default-test";
    $shortcode = "[dms-flexible-ad id='my-default-ad' placement='default-test' vertical='0']";
    $current_user_name = $current_user->display_name;
    $dmsTableName = $wpdb->prefix . 'DMS_FlexiAd';
    $wpdb->insert($dmsTableName, array(
        'adname' => $ad_name,
        'vertical' => $vertical,
        'identifier' => $identifier,
        'adplacement' => $adplacement,
        'shortcode' => $shortcode,
        'current_user_name' => $current_user_name,
        'created_at' => current_time('mysql'),
        'updated_at' => current_time('mysql'),
    ));
    /* Data insert DMS_postdata database table */
    $domain = "pushpros.com";
    $vertical = "3";
    $dmsTableName = $wpdb->prefix . 'DMS_postdata';
    $wpdb->insert($dmsTableName, array(
        'domain' => $domain,
        'vertical' => $vertical,
        'current_user_name' => $current_user_name,
        'created_at' => current_time('mysql'),
        'updated_at' => current_time('mysql'),
    ));
    $refreshValue = 0;
    $refreshdata = serialize(array(
                'refresh' => $refreshValue));
    update_option('refresh_settings', $refreshdata);
}
}
