<?php
function settings()
{
?>
<div class="container containerTab">
		 <div class="row pluginHeader col-lg-12 col-md-12">
	<div class="col-lg-2 col-lg-2 col-sm-4 col-xs-12">
		<img src="<?php echo plugins_url('../image/dms-logo.png', __FILE__); ?>" border="0" class="headerLogo"/>	    
	</div>
	<div class="col-lg-10 col-sm-8">
		<h1 class="adminHeader">
        <?php esc_html_e('Welcome to DMS flexible Advertisement.', 'DMS Flexi Ad'); ?>
		</h1>
	</div>
	</div>
	<div class="row pluginDescription">
	<div class="col-lg-12">
	<h2 class="descriptionHeader">General Settings</h2>
	<hr>
	</div>
	</div>
	<div class="row formlayout">
    <div class="col-lg-12">
	<?php
    $current_user = wp_get_current_user();
    $current_user_name = $current_user->display_name;
    if (isset($_POST['submit']))
    {
        global $wpdb;
        $button_bg_color = $_POST['button_bg_color'];
        $button_text_color = $_POST['button_text_color'];
        if (empty($button_bg_color) || empty($button_text_color))
        {
            echo '<div class="alert alert-danger" role="alert">Settings Not Inserted. Need to fill the form fields</div>';
        }
        else
        {
            $formdata = serialize(array(
                'button_bg_color' => $button_bg_color,
                'button_text_color' => $button_text_color,
                'current_user_name' => $current_user_name,
                'created_at' => current_time('mysql') ,
                'updated_at' => current_time('mysql')
            ));
            update_option('button_settings', $formdata);
            echo '<div class="alert alert-primary" role="alert">Settings Saved Successfully</div>';
        }
    }
    $values = unserialize(str_replace("\n", "\r", get_option('button_settings')));
    if ($values == '')
    { ?>
	<form name="form" action="" method="POST">
	<div class="form-group row formBottom">
            <label for="Identifier"class="col-lg-3 col-form-label">Button Background Color<span class="required">*</span></label>
			<span class="input-help">
            	<div class="row">
			<input type="text" class="form-control col-lg-4" name="button_bg_color" id="Colortxt" aria-describedby="IdHelp" value="#009900" style="width: 350px;">
			<input class="form-control col-lg-1 colors" type = "color" value = "#009900"  id = "color" >
			<span class="colorlabel">Select Color</span>
			</div>
            </span>
          </div>
		  <div class="form-group row formBottom">
            <label for="Identifier"class="col-lg-3 col-form-label">Button Text Color<span class="required">*</span></label>
			<span class="input-help">
				<div class="row">
			<input type="text" class="form-control col-lg-4" name="button_text_color" id="FontColortxt" aria-describedby="IdHelp" value="#000000" style="width: 350px;">
			<input class="form-control col-lg-1 colors" type = "color" value = "#000000"  id = "fontcolor">
			<span class="colorlabel">Select Color</span>
			</div>
			
			</span>
          </div>
		   <small id="IdHelp" class="form-text text-danger helptext"><span class="required">*</span>Color functionality is not supported in Internet Explorer and Safari 9.1 and it's earlier versions.</small>
		   <button name="submit" type="submit" class="btn btn-primary">Save Changes</button>
	</form>
<?php
    }
    elseif ($values != '')
    {
?>
<form name="form" action="" method="POST">
			<div class="form-group row formBottom">
      <label for="Identifier"class="col-lg-3 col-form-label">Select Button Background Color <span class="required">*</span></label>
			<span class="input-help">
			<div class="row">
			<input type="text" class="form-control col-lg-4" name="button_bg_color" id="Colortxt" aria-describedby="IdHelp" value="<?php echo $values['button_bg_color']; ?>" style="width: 350px;">
			<input class="form-control col-lg-1 colors" type = "color" value = "#009900"  id = "color">
			<span class="colorlabel">Select Color</span>
			</div>
            </span>
          </div>
		   <div class="form-group row formBottom">
            <label for="Identifier"class="col-lg-3 col-form-label">Button Text Color<span class="required">*</span></label>
			<span class="input-help">
				<div class="row">
			<input type="text" class="form-control col-lg-4" name="button_text_color" id="FontColortxt" aria-describedby="IdHelp" value="<?php echo $values['button_text_color']; ?>" style="width: 350px;">
			<input class="form-control col-lg-1 colors" type = "color" value = "#000000"  id = "fontcolor">
			<span class="colorlabel">Select Color</span>
			</div>
			
			</span>
          </div>
			<small id="IdHelp" class="form-text text-danger helptext"><span class="required">*</span>Color functionality is not supported in Internet Explorer and Safari 9.1 and it's earlier versions.</small>
		  <button name="submit" type="submit" class="btn btn-primary">Update Changes</button>
	</form>
	</div>
	</div>
<?php
    }
} ?>