<?php
add_shortcode('dms-flexible-ad', 'getFlexiAd');
function getFlexiAd($atts)
{
    global $wpdb;
    $dmsApiSettings = getAPIData();
    $dmsTableName = $wpdb->prefix . 'DMS_FlexiAd';
    $dmsflexiData = $wpdb->get_results("SELECT * FROM $dmsTableName");
    if (!empty($dmsApiSettings[0]->publickey) && !empty($dmsApiSettings[0]->privatekey)) {
        foreach ($dmsflexiData as $flexivalue) {

            if ($flexivalue->adplacement == $atts['placement']) {
                $id = "unique-" . rand_string(10);
                echo '<div class="DmsContainer">
                  <div class="DmsFlexi content-banner banner-wide">
                          <div class="' . $atts['id'] . '" id="' . $id . '">
                           <p class="text-center" style="animation: animate 1.5s linear infinite;">Advertisement Loading ...</p>
                          </div>
                          <script>
                         setTimeout(DMSCallFlexiAdv("' . $id . '","' . $atts['placement'] . '"), 100);
                        </script>
                  </div>
              </div>';
            }
        }

    }
}
