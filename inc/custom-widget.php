<?php
/* Start widget creation */
// The widget class
class Dms_widget extends WP_Widget {
    // Main constructor
    public function __construct() {
        parent::__construct(
            'Dms_widget',
            __( 'DMS Widget', 'text_domain' ),
            array(
                'customize_selective_refresh' => true,
            )
        );
    }

    public function form( $instance ) {
        // Set widget defaults
        $defaults = array(
            //'title'    => '',
            'pages' => [],
            'shortcode_id' => '',
        );

        global $wpdb;
        $dmsTableName = $wpdb->prefix . 'DMS_FlexiAd';
        $dmsflexiData = $wpdb->get_results( "SELECT * FROM $dmsTableName" );
        extract( wp_parse_args( ( array ) $instance, $defaults ) );
        echo $title = $dmsflexiData[0]->title;
        ?>
        <p>
        <?php
        $pages = get_pages(); 
        $allPostsWPQuery = new WP_Query(array('post_type'=> 'post','orderby' => 'ID','post_status' => 'publish','order'  => 'DESC','posts_per_page' => -1)); 
        $posts= $allPostsWPQuery->posts;
        $checkfield ='';
        ?>
        
        <div class="tablink DMS-selected text-center" onclick="showWidgetContent(this, 'shortCode')">ShortCode</div>
        <div class="tablink text-center" onclick="showWidgetContent(this, 'pagesToShow')">Pages to Show</div>
       
        <div id='DMS-Flexi-Module-Shortcode-tab' class="form-check tabcontent" style='display: block;'>

            <!--<label for="Select page">Select the Shortcode</label><br>-->
            <?php
                foreach( $dmsflexiData as $value ){
            ?>
                <input type="radio" name="<?php echo esc_attr( $this->get_field_name( 'shortcode_id' ) ); ?>" value="<?php echo $value->id;?>" <?php echo ($value->id===$instance['shortcode_id'])? 'checked': '';?>>
                <?php echo '<span>'.$value->shortcode.'</span>';?> <br><br>
            <?php
            }
            ?>
        </div>

        <div id='DMS-Flexi-Module-Pagestoshow-tab' class="form-check tabcontentPage" style='display: none;'>

           <!-- <label for="Select page">Select the pages</label><br>-->
           <?php
           $list_of_pages_posts = unserialize($instance['pages']);
                $pages_list=[];
                $posts_list=[];

                foreach($list_of_pages_posts as $value){
                    if('pg-' === substr($value, 0,3)){
                        $pages_list[] = substr($value, 3);
                    }else if('ps-' === substr($value, 0,3)){
                        $posts_list[] = substr($value, 3);
                    }
                }
                ?>

            <select multiple class='DMS_Multi_select_options' name="<?php echo esc_attr( $this->get_field_name( 'pages' ) ); ?>[]" size=10 
                style="padding: 0px !important; font-size: 17px; background: #fff0; border: 1px solid #fff; color: #fff;">
                <optgroup label='Pages'>
                <?php
                foreach($pages as $page_data){
                
                ?>
                <option value = "pg-<?php echo $page_data->ID; ?>" <?php echo in_array($page_data->ID, $pages_list)? 'selected class="selected"': '';?>><?php echo $page_data->post_title; ?></option>
                <?php
                }
                echo '</optgroup>';
                echo '<optgroup label="Posts">';
                foreach($posts as $page_data){
                ?>
                <option value = "ps-<?php echo $page_data->ID; ?>" <?php echo in_array($page_data->ID, $posts_list)? 'selected class="selected"': '';?>><?php echo $page_data->post_title; ?></option>
                <?php
                }  
                ?>
            </optgroup>
            </select>
        </div>
        <div class='show_opted_window'>
            <span style='display: block;background: #17a2b8cf;text-align: center;padding: 10px;color: #003860; font-weight: 500;'>Selected ShortCode</span>
            <div class='opted_short_code'>
            <?php
            foreach( $dmsflexiData as $value ){
                echo ($value->id===$instance['shortcode_id'])? '<span>'.$value->shortcode.'</span>': '';
            }
            ?>
            </div>
            <span style='display: block;background: #17a2b8cf; text-align: center;padding: 10px;color: #003860; font-weight: 500;'>Selected Page(s)</span>
            <div class='opted_pages'>
            <?php 
            foreach($pages as $page_data){
                 echo in_array($page_data->ID, $pages_list)? '<span>'.$page_data->post_title.'</span>': '';
            }
            ?>
            </div>
            <span style='display: block;background: #17a2b8cf; text-align: center;padding: 10px;color: #003860; font-weight: 500;'>Selected Post(s)</span>
            <div class='opted_posts'>
            <?php
             foreach($posts as $page_data){
                 echo in_array($page_data->ID, $posts_list)? '<span>'.$page_data->post_title.'</span>': '';
            }
            ?>
            </div>
        </div>
     
       <!-- <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Widget Title', 'text_domain' ); ?></label>
        <input class="widefat " id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />-->

        </p>
    <?php 
    } 
    
    // Update widget settings
    public function update( $new_instance, $old_instance ) {
        $instance = [];
        //$instance['title']    = isset( $new_instance['title'] ) ? wp_strip_all_tags( $new_instance['title'] ) : '';
        $instance['shortcode_id'] = isset( $new_instance['shortcode_id'] ) ? wp_strip_all_tags( $new_instance['shortcode_id'] ) : '';
        $instance['pages'] = isset( $new_instance['pages'] ) ? serialize( $new_instance['pages'] ) : '';
        return $instance;
    }
    // Display the widget
    public function widget( $args, $instance) {
        //var_dump($instance);
        global $wpdb,$post;
        $dmsApiSettings = getAPIData();
        $dmsTableName = $wpdb->prefix . 'DMS_FlexiAd';
        $dmsflexiData = $wpdb->get_results("SELECT * FROM $dmsTableName");
        $renerShortcode = '';
        $shortcodeid = '';
        foreach($dmsflexiData as $flexidata){
            $shortcodeid = $flexidata->id;
             if($shortcodeid == $instance['shortcode_id']){
            $renerShortcode=$flexidata->shortcode;
        }
        }
        $returnValue = unserialize($instance['pages']);
        //echo $instance['shortcode_id'];
        //echo '<pre>'; print_r($returnValue);
        extract( $args );
        // Check the widget options
        //$title    = isset( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : '';
        $pages    = isset( $instance['pages'] ) ? apply_filters( 'widget_pages', $instance['pages'] ) : '';
        $shortcode_id    = isset( $instance['shortcode_id'] ) ? apply_filters( 'widget_shortcode_id', $instance['shortcode_id'] ) : '';
        echo $before_widget;
        $page_id = get_queried_object_id();
        $current_post_id = $post->ID;
        //echo $current_post_id.'<br>';
        //echo $page_id;
       foreach ($returnValue as $return){
       $selectedpg=substr($return, 3);
        if($selectedpg == $page_id || $selectedpg == $current_post_id) {
           echo do_shortcode("$renerShortcode");
       }

    }
    echo $after_widget;
    }

}

// Register the widget
function my_DMS_widget() {
    register_widget( 'Dms_widget' );
}
add_action( 'widgets_init', 'my_DMS_widget' );
/*End Widget creation */