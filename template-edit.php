<?php
require_once (str_replace('//', '/', dirname(__FILE__) . '/') . '../../../wp-config.php');
global $wpdb;
$table_name = $wpdb->prefix . 'DMS_FlexiAd';
$id = $_POST['id'];
$result = $wpdb->get_results("SELECT * FROM $table_name where id=$id");
$id = $result[0]->id;
$adname = $result[0]->adname;
$vertical = $result[0]->vertical;
$identifier = $result[0]->identifier;
$adplacement = $result[0]->adplacement;
$shortcode = stripslashes($result[0]->shortcode);
$url = "https://services.pushnetwork.com/api/content/verticals?publickey=ljacy9h3&domain=https://1.topfinancefacts.com";
$crl = curl_init();
curl_setopt($crl, CURLOPT_URL, $url);
curl_setopt($crl, CURLOPT_FRESH_CONNECT, true);
curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($crl);
if (!$response)
{
    die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
}
curl_close($crl);
$json = $response;
$json_data = json_decode($json, true);
$var = $json_data['verticals'];
?>
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="descriptionHeader">Edit Advertisement Details</h2>
        <hr>
       <form action="" name="form" id="updateform" method="POST" autocomplete="off">
    <input type="hidden" name="id" value="<?php echo $id; ?>" />
    <div class="form-group row formBottom">
              <label for="Advertisement Name" class="col-lg-2 col-form-label">Advertisement Name</label>
            <input type="text" class="form-control col-lg-5" name="adname" id="adname" value="<?php echo $adname; ?>" aria-describedby="AdHelp"> &nbsp;&nbsp;<span class="Adnameerrmsg"></span>
            </div>
      <div class="form-group row formBottom">
              <label for="Vertical" class="col-lg-2 col-form-label">Vertical</label>
            <select class="form-control col-lg-5" name="vertical" id="vertical" style="max-width: 29rem !important;">
            <option value="0" <?php if ($vertical == '0') echo 'selected="selected"'; ?>>No Preference</option>
             <?php
usort($var, function ($item1, $item2)
{
    if ($item1['score'] == $item2['score']) return 0;
    return $item1['score'] > $item2['score'] ? -1 : 1;
});
?>
            <?php for ($i = 0;$i < count($var);$i++)
{ ?>
            <option value="<?php echo $var[$i]['id']; ?>" <?php if ($vertical == $var[$i]['id']) echo 'selected="selected"'; ?>><?php echo $var[$i]['label']; ?> <?php for ($j = 0;$j < $var[$i]['score'];$j++)
    {
        echo "$";
    } ?></option>
            <?php
} ?>
            </select>
      
          </div>
      <div class="form-group row formBottom">
            <label for="Identifier"class="col-lg-2 col-form-label">Identifier</label>
            <span class="input-help">
            <input type="text" class="form-control col-lg-6" name="identifier" id="identifier" value="<?php echo $identifier; ?>" aria-describedby="IdHelp" style="width: 925px;">
            <small id="IdHelp" class="form-text text-muted">Unique Identifier to be used in the shortcode</small>
            </span>
          </div>
          <div class="form-group row formBottom">
            <label for="Placement"class="col-lg-2 col-form-label">Placement</label>
            <span class="input-help">
            <input type="text" class="form-control col-lg-6" name="adplacement" id="adplacement" value="<?php echo $adplacement; ?>" aria-describedby="IdHelp" style="width: 925px;">
            <small id="IdHelp" class="form-text text-muted">Placement to be used in the shortcode</small>
            </span>
          </div>
          <div class="form-group row formBottom">
            <label for="Shorcode" class="col-lg-2 col-form-label">Shortcode</label>
               <span class="input-help">
            <div class="input-group mb-3 col-sm-12 formPadding">
              <input type="text" class="form-control col-lg-12" value="[pcn-flexible-ad id='<?php echo $identifier; ?>' placement='<?php echo $adplacement; ?>' vertical='<?php echo $vertical; ?>']" name="shortcode" id="Shorcode" aria-describedby="ShortcodeHelp" readonly>
              <div class="input-group-append">
                <input type="button" onclick="copyShortcode()" value="copy" class="btn btn-primary">
              </div>
            </div>
            <small id="ShortcodeHelp" class="form-text text-muted">Click to Copy, then Paste this shortcode into your
              page,post,sidebar,footer,etc.</small>
              </span>
          </div>
          <button name="submit" type="submit" class="btn btn-primary">Update Changes</button>
         </div> 
  </form>
  </div>
  </div>
    </div>
 <script type="text/javascript">
$(document).ready(function() {
     var adname = document.getElementById("adname").value,
        adIdentifier = document.getElementById("identifier").value,
        adPlacement = document.getElementById("adplacement").value,
        sCode,
        end = document.getElementById("vertical").value;
    sCode = "[dms-flexible-ad id='" + adIdentifier + "' placement='" + adPlacement + "' vertical='" + end + "']";
    $('#Shorcode').val(sCode);
    $("#adname,#identifier").keyup(function() {
        $('#identifier').val((this.value).replace(/\s+/g, '-').toLowerCase());
        adIdentifier = $('#identifier').val();
        sCode = "[dms-flexible-ad id='" + adIdentifier + "' placement='" + adPlacement + "' vertical='" + end + "']";
        $('#Shorcode').val((sCode).toLowerCase());
    });
    $('#adplacement').keyup(function() {
        adPlacement = $('#adplacement').val();
        console.log(adPlacement);
        sCode = "[dms-flexible-ad id='" + adIdentifier + "' placement='" + adPlacement + "' vertical='" + end + "']";
        $('#Shorcode').val((sCode).toLowerCase());
    });
    
    $("#vertical").change(function() {
        end = this.value;
        sCode = "[dms-flexible-ad id='" + adIdentifier + "' placement='" + adPlacement + "' vertical='" + end + "']";
        $('#Shorcode').val((sCode).toLowerCase());
    });
});
$(document).ready(function() {
    $("#adname").keypress(function(e) {
        var keyCode = e.which;
        if (!((keyCode >= 48 && keyCode <= 57) ||
                (keyCode >= 65 && keyCode <= 90) ||
                (keyCode >= 97 && keyCode <= 122)) &&
            keyCode != 8 && keyCode != 32) {
            e.preventDefault();
            $(".Adnameerrmsg").html("Doesn't allow Special characters").show().fadeOut(2500);
            return false;
        }
    });
});
</script>