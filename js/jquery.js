$(document).ready(function() {
     var adIdentifier = '',
        adPlacement ='',
        sCode,
        end = 0;
    sCode = "[dms-flexible-ad id='" + adIdentifier + "' placement='" + adPlacement + "' vertical='" + end + "']";
    $('#Shorcode').val(sCode);
    $("#adname,#identifier").keyup(function() {
        $('#identifier').val((this.value).replace(/\s+/g, '-').toLowerCase());
        adIdentifier = $('#identifier').val();
        sCode = "[dms-flexible-ad id='" + adIdentifier + "' placement='" + adPlacement + "' vertical='" + end + "']";
        $('#Shorcode').val((sCode).toLowerCase());
    });
    $('#adplacement').keyup(function() {
        adPlacement = $('#adplacement').val();
        //console.log(adPlacement);
        sCode = "[dms-flexible-ad id='" + adIdentifier + "' placement='" + adPlacement + "' vertical='" + end + "']";
        $('#Shorcode').val((sCode).toLowerCase());
    });
    $("#vertical").change(function() {
        end = this.value;
        sCode = "[dms-flexible-ad id='" + adIdentifier + "' placement='" + adPlacement + "' vertical='" + end + "']";
        $('#Shorcode').val((sCode).toLowerCase());
    });


});

function copyShortcode() {
    var copyCode = document.getElementById("Shorcode");
    copyCode.select();
    document.execCommand("copy");
}

$(document).ready(function() {
    $("#adname").keypress(function(e) {
        var keyCode = e.which;
        if (!((keyCode >= 48 && keyCode <= 57) ||
                (keyCode >= 65 && keyCode <= 90) ||
                (keyCode >= 97 && keyCode <= 122)) &&
            keyCode != 8 && keyCode != 32) {
            e.preventDefault();
            $(".Adnameerrmsg").html("Doesn't allow Special characters").show().fadeOut(2500);
            return false;
        }
    });
    $("#height").keypress(function(e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $(".errmsg").html("Allow Numbers Only").show().fadeOut(2500);
            return false;
        }
    });
    $("#width").keypress(function(e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $("#errmsg").html("Allow Numbers Only").show().fadeOut(2500);
            return false;
        }
    });

});