var vvr;
$(document).ready(function(){

    jQuery('#DMS-Flexi-Module-Pagestoshow-tab option').mousedown(function(e){
        e.preventDefault();
        jQuery(this).toggleClass('selected');

        jQuery(this).prop('selected', !jQuery(this).prop('selected'));
            
        jQuery(this).closest('form').find('input:submit').prop('disabled', false).val('Save');//save option not worked, so this code will activate save button when options selected
        if($(this).closest('optgroup').attr('label')==='Pages'){
            jQuery(this).closest('#DMS-Flexi-Module-Pagestoshow-tab').siblings('.show_opted_window').children('.opted_pages').html(getTextFromVales(jQuery(this).parent().children().map(function(){ vvr=$(this);
                if( $(this).hasClass('selected')){
                    return $(this).text(); 
                }
            })));
        }else if($(this).closest('optgroup').attr('label')==='Posts'){
            jQuery(this).closest('#DMS-Flexi-Module-Pagestoshow-tab').siblings('.show_opted_window').children('.opted_posts').html(getTextFromVales(jQuery(this).parent().children().map(function(){ vvr=$(this);
                if( $(this).hasClass('selected')){
                    return $(this).text(); 
                }
            })));
        }
        return false;
    });

    //show opted shortcode in the bottom window
    jQuery('#DMS-Flexi-Module-Shortcode-tab [type="radio"]').change(function(){
        jQuery(this).parent().siblings('.show_opted_window').children('.opted_short_code').html('<span>'+jQuery(this).next('span').html()+'</span>');
    });
});

function getTextFromVales(args){
    var html='';
    if(args.length !== 0){
        for(var x = 0; x < args.length; x++){
       
            html += '<span class="DMS-Flexi-format">' + args[x] + '</span>';
        }
    } else {
        html = '<span class="DMS-Flexi-format">No Pages Selected</span>';

    }
    return html;
}