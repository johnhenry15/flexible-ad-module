"use strict";
class DMSFlexibleAdModule {
    DMSFetchData = function(advclassName = null, placement = null) {
        var DMSContentDesc, DMSContentAdvertisement, DMSContentLink, DMSContentTitle;
        var formdata = {
            "domain": document.getElementById("dms_domain").value,
            "placement": placement,
            "vertical": document.getElementById("dms_verticaldata").value,
            "ua": navigator.userAgent,
            "uid": document.getElementById("dms_ipaddress").value,
            "geo": document.getElementById("dms_geo").value,
            "btnbgcolor":document.getElementById("button_color").value,
            "btntxtcolor":document.getElementById("button_text_color").value
        }; console.log(formdata);
        fetch('https://get.pushnetwork.com/api/content', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'userid': document.getElementById("dms_publickey").value
            },
            body: JSON.stringify(formdata)
        }).then((response) => response.json()).then(function(data) {
            var width = document.getElementById(advclassName).offsetWidth;
            if(width > 150 && width < 300) {
                var customStyle = "mobilead";
                var colstyle = "col-md-12";
                var colstyle1 = "";
            } else if(width < 150 ) {
                var customStyle = "longad";
                var colstyle = "col-md-10";
                var colstyle1 = "col-md-2";
            } 
            else if(width > 800 && width < 1024) {
                var customStyle = "desktopad";
                var colstyle = "col-md-10";
                var colstyle1 = "col-md-2";
            } else if(width <= 768) {
                var customStyle = "tabview";
                var colstyle = "col-md-12";
                var colstyle1 = "";
            }
            var myJson = data.result;
            DMSContentTitle = '<div class="' + customStyle + '"><a href="' + myJson.link + '"><div class="DmsFlexi BannerContainer" style="background: linear-gradient(rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35)), url(' + myJson.customIcon + ') no-repeat center;background-size: cover;"><div style="position: absolute; bottom: 0px; width: 100%; left: 0px;background: #00000059; padding: 10px 0px 10px 0px;"><div class="container"><div class="row"><div class="' + colstyle + ' col-auto"><a href="' + myJson.link + '"><h3 class="ContentSectionTitle">' + myJson.title + '</h3></a>';
            DMSContentDesc = '<h4 class="ContentSectionSubtitle">' + myJson.body.substring(0, 50) + '</h4></a></div>';
            DMSContentLink = '<div class="' + colstyle1 + ' col-auto"><a target="_blank" alt="image link"  href="' + myJson.link + '"class="learnmore1" style="background-color:'+ formdata.btnbgcolor +'; color:'+ formdata.btntxtcolor +'">Learn More</a></div></div></div></div></a></div>';
            DMSContentAdvertisement = DMSContentTitle + DMSContentDesc + DMSContentLink;
            document.getElementById(advclassName).innerHTML = DMSContentAdvertisement;
        }).catch(function(err) {
            //console.log(err)
            //  console.log('API data not retrived!' + err.success);
        });
    };
}

function DMSCallFlexiAdv(advclassName, placement) {
    // below obj are response json of AD apis
    // creation of ad instances , parameters: ad_API_object, id_name, width, height 
    const ad02 = new DMSFlexibleAdModule({}, 'ad_container2', 100, 100);
    // ad  methods  called on ad instances
    ad02.DMSFetchData(advclassName, placement);
}