function findSiblingId(e, dmsId) {
    var siblings = e.parentNode.children,
    sibWithId = null;
    for(var i = siblings.length; i--;) {
        if(siblings[i].id === dmsId) {
            sibWithId = siblings[i];
            break;
        }
    }
    return sibWithId;
}
function showWidgetContent(e, whichDmsButton) {
    if( whichDmsButton === 'shortCode' ){
        siblingWithId = findSiblingId(e, 'DMS-Flexi-Module-Shortcode-tab');
        if( 'null' !== siblingWithId && siblingWithId.style.display==='none' ){
            e.classList.toggle('DMS-selected');
            e.nextElementSibling.classList.toggle('DMS-selected');
            siblingWithId.style.display = 'block';
            siblingWithId.nextElementSibling.style.display = 'none';
        }
    }else if( whichDmsButton === 'pagesToShow' ){
        siblingWithId = findSiblingId(e, 'DMS-Flexi-Module-Pagestoshow-tab');
        if('null' !== siblingWithId && siblingWithId.style.display === 'none' ){
            e.classList.toggle('DMS-selected');
            e.previousElementSibling.classList.toggle('DMS-selected');
            siblingWithId.style.display = 'block';
            siblingWithId.previousElementSibling.style.display = 'none';
        }

    }
    
};
